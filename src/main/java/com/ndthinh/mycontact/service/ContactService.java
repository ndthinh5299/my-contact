package com.ndthinh.mycontact.service;

import com.ndthinh.mycontact.entity.Contact;

import java.util.List;

public interface ContactService {

    Iterable<Contact> findAll();

    List<Contact> search(String term);

    Contact findOne(Integer id);

    void save(Contact contact);

    boolean delete(Integer id);

}
