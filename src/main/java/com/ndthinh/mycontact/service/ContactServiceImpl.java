package com.ndthinh.mycontact.service;

import com.ndthinh.mycontact.entity.Contact;
import com.ndthinh.mycontact.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactServiceImpl  implements ContactService {
    @Autowired
    private ContactRepository contactRepository;

    @Override
    public Iterable<Contact> findAll() {
        return contactRepository.findAll();
    }

    @Override
    public List<Contact> search(String term) {
        return contactRepository.findByNameContaining(term);
    }

    @Override
    public Contact findOne(Integer id) {
        Optional<Contact> contact = contactRepository.findById(id);
        if(contact.isPresent()){
            return contact.get();
        }
        return null;
    }

    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
    }

    @Override
    public boolean delete(Integer id) {
        Contact contact = findOne(id);
        if (contact != null) {
            contactRepository.delete(contact);
            return true;
        }
        return false;
    }
}
